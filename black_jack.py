"""
Simple Black Jack RL environnement
"""
# Authors: Hamza Cherkaoui <hamza.cherkaoui@inria.fr>
# License: BSD (3-clause)
import numpy as np

possible_actions = ['stay', 'hit']
init_deck = list(range(1, 14)) * 4
penalization_score = -1


def get_card_score(card_number, high_ace=True):
    """ Return value of card.
    """
    if card_number == 1:
        if high_ace:
            return 10
        else:
            return 1
    elif card_number in list(range(2, 11)):
        return card_number
    else:
        return 10


class BlackJackPlayer():
    """Simple BlacJack player for RL."""

    def __init__(self):

        # keep record of dealer/player cards
        self.dealer_cards = []
        self.player_cards = []

        # keep record of dealer/player score
        self.player_score = 0
        self.dealer_score = 0

        # high ace
        self.high_ace = True

    def choose_action(self, observation):

        dealer_new_card = observation['dealer']
        player_new_card = observation['player']

        self.dealer_cards.append(dealer_new_card)
        self.dealer_score += get_card_score(dealer_new_card,
                                            high_ace=self.high_ace)

        self.player_cards.append(player_new_card)
        self.player_score += get_card_score(player_new_card,
                                            high_ace=self.high_ace)

        # XXX change player policy
        coin = np.random.binomial(size=1, n=1, p=0.25)[0]
        action = 'stay' if coin == 0 else 'hit'
        coin = np.random.binomial(size=1, n=1, p=0.75)[0]
        ace_value = True if coin == 0 else False

        self.high_ace = ace_value

        return (action, ace_value)


class BlackJackEnv():
    """Simple BlacJack environnement for RL."""

    def __init__(self):

        # avalaible cards to draw
        self.current_deck = []

        # keep record of dealer/player cards
        self.dealer_cards = []
        self.player_cards = []

        # keep record of dealer/player score
        self.player_score = 0
        self.dealer_score = 0

        # high ace
        self.high_ace = True

    def step(self, actions):

        dealer_action = actions['dealer']
        player_action = actions['player']
        player_action, self.high_ace  = player_action

        if dealer_action == 'hit':
            dealer_new_card = self._draw_one_card_at_random()
            self.dealer_cards.append(dealer_new_card)
            self.dealer_score += get_card_score(dealer_new_card,
                                                high_ace=self.high_ace)
        else:
            dealer_new_card = None

        if player_action == 'hit':
            player_new_card = self._draw_one_card_at_random()
            self.player_cards.append(player_new_card)
            self.player_score += get_card_score(player_new_card,
                                                high_ace=self.high_ace)
        else:
            player_new_card = None

        observation = dict(dealer=dealer_new_card, player=player_new_card)
        reward = (self.player_score if self.player_score <= 21
                                    else penalization_score)

        if ((self.player_score > 21)
            or (self.dealer_score > 21)
            or ((player_action == 'hit') and (dealer_action == 'hit'))):
            done = True
        else:
            done = False

        info = dict(dealer_cards=self.dealer_cards,
                    player_cards=self.player_cards,
                    player_score=self.player_score,
                    dealer_score=self.dealer_score,
                    high_ace=self.high_ace,
                    done=done)

        return observation, reward, done, info

    def choose_action(self, observation):
        if self.dealer_score <= 17:
            return 'hit'
        else:
            return 'stay'

    def reset(self):
        # reset deck
        self.current_deck = init_deck

        # player / dealer give back cards
        self.dealer_cards = []
        self.player_cards = []

        # score at 0
        self.player_score = 0
        self.dealer_score = 0

        self.high_ace = True

        # draw player cards
        first_card = self._draw_one_card_at_random()
        second_card = self._draw_one_card_at_random()
        self.player_cards.append(first_card)
        self.player_cards.append(second_card)
        self.player_score += get_card_score(first_card, high_ace=True)
        self.player_score += get_card_score(second_card, high_ace=True)

        # draw dealer cards
        first_card = self._draw_one_card_at_random()
        second_card = self._draw_one_card_at_random()
        self.dealer_cards.append(first_card)
        self.dealer_cards.append(second_card)
        self.dealer_score += get_card_score(first_card, high_ace=True)
        self.dealer_score += get_card_score(second_card, high_ace=True)

        observation = dict(dealer=self.dealer_cards, player=self.player_cards)

        info = dict(dealer_cards=self.dealer_cards,
                    player_cards=self.player_cards,
                    player_score=self.player_score,
                    dealer_score=self.dealer_score,
                    high_ace=self.high_ace,
                    done=False)

        return observation, info

    def _draw_one_card_at_random(self):
        idx_to_pop = np.random.randint(len(self.current_deck))
        return self.current_deck.pop(idx_to_pop)


if __name__ == '__main__':

    dealer = BlackJackEnv()
    player = BlackJackPlayer()

    n_episode, n_step = 2, 5
    debug = True

    for episode in range(n_episode):
        observation, info = dealer.reset()  # info can help here

        for step in range(n_step):

            # dealer and player take decision to take a card or not
            player_action = player.choose_action(observation)
            dealer_action = dealer.choose_action(observation)

            # definition of all the actions for this turn
            actions = dict(player=player_action, dealer=dealer_action)

            # update the game status
            observation, reward, done, info = dealer.step(actions)

            if debug:
                print("-" * 10)
                print(f"[s:{step+1:2d} - ep:{episode+1:2d}] "
                      f" player score: {info['player_score']}"
                      f" | dealer score: {info['dealer_score']}"
                      f" | high-ace?: {info['high_ace']}"
                      f" | done?: {info['done']}")
                print(f"player cards: {info['player_cards']}")
                print(f"dealer cards: {info['dealer_cards']}")

            if done:
                break

        if debug:
            print("*" * 20)